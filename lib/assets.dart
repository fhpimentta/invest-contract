class Assets {
  // Images
  static const logo = 'assets/images/logo.png';
  static const logoHorizontal = 'assets/images/logo-transparent.png';
  static const onBoardingStep1 = 'assets/images/onboarding/step1.jpg';
  static const onBoardingStep2 = 'assets/images/onboarding/step2.jpg';
  static const onBoardingStep3 = 'assets/images/onboarding/step3.jpg';
  static const onBoardingStep4 = 'assets/images/onboarding/step4.jpg';
}
