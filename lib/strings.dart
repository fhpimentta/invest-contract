class Strings {
  // Common
  static const String unexpectedError = "Erro inesperado";
  static const String unexpectedErrorDescription =
      "Infelizmente ocorreu um erro inesperado, tente novamente mais tarde";
  static const String invalidEmail = "E-mail inválido";
  static const String logout = "Sair";
  static const String unableToLogout = "Não foi possível sair do aplicativo";

  // Splash Page
  static const String contratos = " Contratos";

  // Onboarding Page
  static const String onboardingStep01Label =
      "Olá!\nAgora você tem o jeito\nmais fácil de investir em \Contratos";
  static const String onboardingStep02Title = "Assinatura Zero";
  static const String onboardingStep02Description =
      "Aproveite para criar contratos com\n Zero de gastos, em qualquer\ntipo de ramo";
  static const String onboardingStep03Title =
      "Cashback em Fundos de\n Contratos";
  static const String onboardingStep03Description =
      "Receba parte da taxa de\nadministração, em dinheiro, direto\nna sua contract system.";
  static const String onboardingStep04Title = "E tem muito mais!";
  static const String onboardingStep04Tip01 = "Recomendações de contratos";
  static const String onboardingStep04Tip02 = "Cursos do iniciante ao avançado";
  static const String onboardingStep04Tip03 =
      "Crie contratos se forma simples";
  static const String openYourAccount = "Abrir conta";
  static const String signIn = "Entrar";

  // Login
  static const String theEasiestWayTo = "O jeito mais fácil de";
  static const String invest = "criar contrato";
  static const String stockExchanges = "na Contratação";
  static const String cpf = "Cpf";
  static const String invalidCpf = "CPF inválido";
  static const String password = "Senha";
  static const String dontHaveAccount = "Não tem uma conta?";
  static const String signUp = "Registre-se";
  static const String email = "E-mail";
  static const String forgotPassword = "Esqueceu sua senha?";
}
